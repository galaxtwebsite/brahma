FROM node:10.16.0
WORKDIR /product
COPY package.json /product
RUN npm install
COPY . /product
CMD [ "node","server/server.js" ]
EXPOSE 3000